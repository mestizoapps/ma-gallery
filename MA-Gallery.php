<?php
/*
Plugin Name: GS Gallery
Plugin URI: http://mestizoapps.com/plugins/gs-gallery
Description: Gestor de galerias desarrollado por M-APPS.
Version: 1.0
Author: Mestizo Apps
Author URI: http://mestizoapps.com
*/

define("SPEE_PLUGIN_URL", WP_PLUGIN_URL.'/'.basename(dirname(__FILE__)));
define("SPEE_PLUGIN_DIR", WP_PLUGIN_DIR.'/'.basename(dirname(__FILE__)));

//Agregamos los archvios de estilos del plugin en el head de wordpress
function add_gs_head(){
    wp_enqueue_style('gs-style', plugins_url("MA-Gallery/include/css/").'ma-gallery.css'); 
    wp_enqueue_script('gs-script', plugins_url("MA-Gallery/include/js/").'ma-gallery.js');
}

add_action('wp_head', 'add_gs_head');

function gs_gallery_post(){

    $gs_gallery_labels = array(
		'name' 					=> esc_html__( 'GS Gallery', 'M-Apps' ),
		'singular_name' 		=> esc_html__( 'Gallery', 'M-Apps' ),
		'add_new' 				=> esc_html__( 'Añadir Nueva', 'M-Apps' ),
		'add_new_item' 			=> esc_html__( 'Añadir nueva Galería', 'M-Apps' ),
		'edit_item' 			=> esc_html__( 'Editar Galería', 'M-Apps' ),
		'new_item' 				=> esc_html__( 'Nueva Galería', 'M-Apps' ),
		'all_items' 			=> esc_html__( 'Galerias', 'M-Apps' ),
		'view_item' 			=> esc_html__( 'Ver Galería', 'M-Apps' ),
		'search_items' 			=> esc_html__( 'Buscar Galería', 'M-Apps' ),
		'not_found' 			=> esc_html__( 'No se encontro Galería', 'M-Apps' ),
		'not_found_in_trash' 	=> esc_html__( 'No se encontro Galería en la pepelera', 'M-Apps' ), 
		'parent_item_colon' 	=> ''
	);
	
	$gallery_args = array(
		'labels' 				=> $gs_gallery_labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'show_in_menu'       	=> true,
		'query_var' 			=> true,
		'rewrite' 				=> array( 'with_front' => false, 'slug' => 'gs-gallery' ),
		'capability_type' 		=> 'post',
		'hierarchical' 			=> false,
		'has_archive' 			=> false,
		'exclude_from_search' 	=> true,
		'supports' 				=> array( 'title', 'thumbnail', 'editor' )
	);
	
	if( ! post_type_exists('gs_gallery') ) {
		register_post_type( 'gs_gallery', $gallery_args );
	}

	$gs_gallery_category_labels = array(
		'name'              	=> esc_html__( 'Categorias', 'M-Apps' ),
		'singular_name'     	=> esc_html__( 'Categoría', 'M-Apps' ),
		'search_items'      	=> esc_html__( 'Search Categories', 'M-Apps' ),
		'all_items'         	=> esc_html__( 'Todas las categorias', 'M-Apps' ),
		'parent_item'       	=> esc_html__( 'Categoría Padre', 'M-Apps' ),
		'parent_item_colon' 	=> esc_html__( 'Categoría Padre:', 'M-Apps' ),
		'edit_item'         	=> esc_html__( 'Editar Categoria', 'M-Apps' ),
		'update_item'       	=> esc_html__( 'Actualizar Categoria', 'M-Apps' ),
		'add_new_item'      	=> esc_html__( 'Añadir Nueva Categoría', 'M-Apps' ),
		'new_item_name'     	=> esc_html__( 'Nombre Nueva Categoría', 'M-Apps' ),
		'menu_name'         	=> esc_html__( 'Categorias', 'M-Apps' ),
	);

	$gs_gallery_category_args = array(
		'hierarchical'      	=> true,
		'labels'            	=> $gs_gallery_category_labels,
		'show_ui'           	=> true,
		'show_admin_column' 	=> true,
		'show_in_nav_menus' 	=> false,
		'query_var'         	=> true,
		'rewrite'           	=> array( 'with_front' => false, 'slug' => 'gs-gallery-categories' ),
	);
	
	if( ! taxonomy_exists( 'gs_gallery_categories' ) ) {
		register_taxonomy( 'gs_gallery_categories', 'gs_gallery', $gs_gallery_category_args );
	}

	$gs_gallery_skills_labels = array(
		'name'              	=> esc_html__( 'Tags', 'M-Apps' ),
		'singular_name'     	=> esc_html__( 'Tag', 'M-Apps' ),
		'search_items'      	=> esc_html__( 'Search Tags', 'M-Apps' ),
		'all_items'         	=> esc_html__( 'All Tags', 'M-Apps' ),
		'parent_item'       	=> esc_html__( 'Parent Tag', 'M-Apps' ),
		'parent_item_colon' 	=> esc_html__( 'Parent Tag:', 'M-Apps' ),
		'edit_item'         	=> esc_html__( 'Edit Tag', 'M-Apps' ),
		'update_item'       	=> esc_html__( 'Update Tag', 'M-Apps' ),
		'add_new_item'      	=> esc_html__( 'Add New Tag', 'M-Apps' ),
		'new_item_name'     	=> esc_html__( 'New Tag', 'M-Apps' ),
		'menu_name'         	=> esc_html__( 'Tags', 'M-Apps' ),
	);

	$gs_gallery_skills_args = array(
		'hierarchical'      	=> true,
		'labels'            	=> $gs_gallery_skills_labels,
		'show_ui'           	=> true,
		'show_admin_column' 	=> true,
		'show_in_nav_menus' 	=> false,
		'query_var'         	=> true,
		'rewrite'           	=> array( 'with_front' => false, 'slug' => 'gs-gallery-skills' ),
	);
	
	if( ! taxonomy_exists( 'gs_gallery_skills' ) ) {
		register_taxonomy( 'gs_gallery_skills', 'gs_gallery', $gs_gallery_skills_args );
	}

	flush_rewrite_rules();
}

add_action( 'init', 'gs_gallery_post');



function gs_gallery_shortcode( $atts ) {
	$loop; global $paged;  $content;

	if (isset($atts['post'])) {
		$posts_per_page = $atts['post'];
	}else{
		$posts_per_page = 9;
	}
	
	if (isset($atts['category'])) { 
		$gs_term = $atts['category'];
	}

	if(isset($atts['columns'])){
		$cols = 12/$atts['columns']; 
		$width = '1100'/$atts['columns']; 
	}else{
		$cols = 3;
		$width = '1100'/$cols;
	}

	
	$height = $width* 0.56;

	$args = array( 
				'post_type'   => 'gs_gallery', 
				'post_status' => 'publish', 
				'showposts' => $posts_per_page, 
				'paged' => $paged,
				'tax_query' => array(
       				array(
           				'taxonomy' => 'gs_gallery_categories',
           				'field' => 'slug',
                		'terms' => $gs_term 
                		 
            		)
   				) 
   			);
 
	$gs_gallery = new WP_Query( $args );

	$total_found_posts = $gs_gallery->found_posts;
    $total_page = ceil($total_found_posts / $posts_per_page);
     
	if( $gs_gallery->have_posts() ) :
	$content .='	
	  <div class="gs-container container">
	  	<div class="gs-row">';
	     while( $gs_gallery->have_posts() ) : $gs_gallery->the_post(); 
	$content .='<a href="'.get_permalink().'">
	          <div col="'. $cols.'"  class="gs-card" style="width:'.$width.'px; height:'. $height.'px;" id="post-'.get_the_ID().'">
	          	<div class="gs-card-title" style="width:calc('.$width.'px * 0.96); height:'.$height.'px;"><h3>'.get_the_title().'</h3></div>
	          	<div class="gs-card-image" style="background-image: url('.get_the_post_thumbnail_url().'); background-size:cover; background-position: center;  width:'.$width.'px; height:'. $height.'px;" ></div>
	          </div>
	        </a>
	';?>
		<?php endwhile; wp_reset_postdata(); ?>
		
		<?php 
			if(function_exists('wp_pagenavi')) {
        		$list .='<div class="page-navigation">'.wp_pagenavi(array('query' => $post_query, 'echo' => false)).'</div>';

    		} else {
        		$list.='<li class="prev page-numbers">'.get_previous_posts_link('<i class="fa fa-angle-left"></i>');
        		
        		if($paged >0){
        			$list .='<li class="prev page-numbers"><a href="#">'.$paged.'</a></li>'; 
        	 	}
        		
        		if($paged < $total_page){
        			$list  .='</li><li class="next page-numbers">'.get_next_posts_link('<i class="fa fa-angle-right"></i>', $total_page).'</li>';
        		}
        	}
        	$content .= '<div class="col-xs-12"><ul class="pagination scroll-pagination">'.$list.'</ul></div>	
		</div>
	  </div>';
	  return $content;

	else : echo '<div class="gs-container container"><h4>No hay galeria a mostrar.</h4><div>'; endif;
}

add_shortcode('gs_gallery', 'gs_gallery_shortcode');


function gs_gallery_footer_shortcode( $atts ) {
	$loop; 

	if (isset($atts['post'])) {
		$posts_per_page = $atts['post'];
	}else{
		$posts_per_page = 4;
	}

	$args = array( 'post_type'   => 'gs_gallery', 'post_status' => 'publish', 'showposts' => $posts_per_page);
 
	$gs_footer_gallery = new WP_Query( $args );

	$total_found_posts = $gs_footer_gallery->found_posts;
    $total_page = ceil($total_found_posts / $posts_per_page);

	if( $gs_footer_gallery->have_posts() ) :?>

	  <div class="gs-container container">
	    <?php while( $gs_footer_gallery->have_posts() ) : $gs_footer_gallery->the_post(); ?>
	    	<a href="<?php echo get_permalink();?>">
	          <div class="gs-card col-xs-6"  id="post-<?php echo the_ID(); ?>">
	          	<div class="gs-card-title" ><h3><?php print get_the_title(); ?></h3></div>
	          	<div class="gs-card-image" ></div>
	          	<!--div class="gs-card-content"><?php // print get_the_content();  ?></div-->
	          </div>
	        </a>
		<?php endwhile; wp_reset_postdata(); ?> 
	  </div>

	<?php endif;
}

add_shortcode('gs_gallery_footer', 'gs_gallery_footer_shortcode');

class gs_gallery_widget extends WP_Widget {
	// Main constructor
	public function __construct() {
		parent::__construct(
			'gs_gallery_widget',
			__( 'GS Gallery', 'text_domain' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}
	// The widget form (for the backend )
	public function form( $instance ) {
		// Set widget defaults
		$defaults = array(
			'title'    => '',
			'gs_number'=> '',
			'textarea' => '',
			'to_url'   => '',
			'gs_category'   => '',
		);
		
		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Titulo', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
 		 
		<?php $category = get_terms('gs_gallery_categories');  ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'gs_category' ); ?>"><?php _e( 'Categoría', 'text_domain' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'gs_category' ); ?>" id="<?php echo $this->get_field_id( 'gs_category' ); ?>" class="widefat">
			<?php
				if(strlen($gs_category)> 1){
					echo '<option value="'.$gs_category.'">'.get_the_category_by_ID($gs_category).'</option>';
					echo '<option value="">No Mostrar</option>';
				}else{ echo '<option value="">Seleccione una Categoría</option>'; }?>
				<optgroup label="Categorias">
					<?php foreach ($category as $catVal) { 
						echo '<option value="'.$catVal->term_id.'">'.$catVal->name.'</option>'; 
						 
					} ?>	 
				</optgroup>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'gs_number' ) ); ?>"><?php _e( 'Numero de Imagenes', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'gs_number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'gs_number' ) ); ?>" type="number" value="<?php if($gs_number > 0 ){ echo $number; } ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'to_url' ) ); ?>"><?php _e( 'Abri enlace', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'to_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'to_url' ) ); ?>" type="checkbox" value="1" <?php if($to_url  == 1 ){ echo "checked"; } ?> />
		</p>
		

	<?php }
	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['gs_number'] = isset( $new_instance['gs_number'] ) ? wp_strip_all_tags( $new_instance['gs_number'] ) : '';
		$instance['textarea'] = isset( $new_instance['textarea'] ) ? wp_kses_post( $new_instance['textarea'] ) : '';
		$instance['to_url']   = isset( $new_instance['to_url'] ) ? 1 : false;
		$instance['gs_category']   = isset( $new_instance['gs_category'] ) ? wp_strip_all_tags( $new_instance['gs_category'] ) : '';
		return $instance;
	}
	// Display the widget
	public function widget( $args, $instance ) {
		extract( $args );
		// Check the widget options
		$title    	 = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$gs_category = isset( $instance['gs_category'] ) ? $instance['gs_category'] : ''; 
		$gs_number   = isset( $instance['gs_number'] ) ? $instance['gs_number'] : ''; 
		$to_url      = isset( $instance['to_url'] ) ? $instance['to_url'] : ''; 
		// WordPress core before_widget hook (always include )
		echo $before_widget;
		// Display the widget
		echo '<div class="widget-text wp_widget_plugin_box">';
			// Display widget title if defined
			if ( $title ) {
				echo $before_title . $title . $after_title;
			}
			// Display select field 
			if ( $gs_category ) { 

				$args = array( 
							'post_type' => 'gs_gallery', 
							'post_status' => 'publish', 
							'showposts' =>  $gs_number,
							'tax_query' => array(
       							 array(
                					'taxonomy' => 'gs_gallery_categories',
                					'terms' => $gs_category
            					)
        					) 
        				);
				$gs_gallery = new WP_Query( $args ); ?>

				<?php while( $gs_gallery->have_posts() ) : $gs_gallery->the_post(); ?>
	    		<?php if ( $to_url ) { ?><a href="<?php echo get_permalink();?>"><?php } ?> 
	          		<div class="gs-card-footer" style="width:48%; margin:1%; float:left" id="post-<?php echo the_ID(); ?>"> 
	          			<div class="gs-card-image-gooter"  style="height:70px; background-image: url(<?php echo the_post_thumbnail_url(); ?>); background-size:cover; background-position: center;" ></div>
	          		</div>
	        	<?php if ( $to_url ) { ?></a><?php } ?> 
				<?php endwhile; wp_reset_postdata();  
			}
			 
		echo '</div>';
		// WordPress core after_widget hook (always include )
		echo $after_widget;
	}
}
// Register the widget
function register_gs_gallery_widget() {
	register_widget( 'gs_gallery_widget' );
}
add_action( 'widgets_init', 'register_gs_gallery_widget' );

function vc_gs_gallery() {
   		/**
   		* 	Aquí pondremos el código para generar
   		*	el módulo de Visual Composer
   		*/
   		$cat=[];
   		$category = get_terms('gs_gallery_categories');  
   		foreach ($category as $catVal) {
   			array_push($cat, $catVal->name);
   		}
   		vc_map([
        	"category" => "GS",
        	"name" => "GS Gallery",
        	"base" => "gs-gallery",
        	"description" => "Este módulo gestiona las galerias del plugin GS Gallery",
        	"show_settings_on_create" => true,
        	"class" => "vc-project-details-block",
        	"icon" => "icon-wpb-slideshow",
        	"params" => array(
	
        	                    // Campo para el color del texto
                            array(
                                "heading"       => "Seleccione una categoría",
                                "type"          => "dropdown",
                                "param_name"    => "category",
                                "description"   => "Elige la categoria de la galeria a mostrar",
                                "value"         => $cat,
                                "admin_label"   => true
                            ),


                            // Campo para el color del fondo
                            array(
                                "heading"       => "Imagenes por página",
                                "type"          => "textfield",
                                "param_name"    => "post",
                                "description"   => "Elige el numero de imagenes a mostrar por página.",
                                "value"         => "9",
                                "admin_label"   => true
                            ),


                            // Campo para el tamaño del texto
                            array(
                                "heading"           => "Columnas",
                                "type"              => "textfield",
                                "param_name"        => "columns",
                                "description"       => "Elige el número de columnas a mostrar",
                                "value"             => "2",
                                "admin_label"       => true
                            ),

                        )
   		]);

	}
	add_action( 'vc_before_init', 'vc_gs_gallery' );